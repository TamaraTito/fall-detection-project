# Goal of the Project

This project aims to accurately identify and classify falls experienced by elderly individuals.

## Built with :


Python 3.14

## Libraries used:


Sklearn
Matplotlib
Pandas
XGBoost
math
datetime 
statistics

## Data Samples size:

17298 observations and 9 features 